import { Component, ViewChild } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'AppinessTask';
  formData: any;
  ItemList: any;
  @ViewChild("modalDefault", { static: true }) modalDefault;
  settings = {
    actions: {
      add: false,
      position: 'right',
      custom: [
        {
          name: 'ViewAction',
          title: 'View '
        }
      ],
    },
    // add: {
    //   addButtonContent: '<i class="nb-plus"></i>',
    //   createButtonContent: '<i class="nb-checkmark"></i>',
    //   cancelButtonContent: '<i class="nb-close"></i>',
    // },
    // edit: {
    //   editButtonContent: '<i class="nb-edit"></i>',
    //   saveButtonContent: '<i class="nb-checkmark"></i>',
    //   cancelButtonContent: '<i class="nb-close"></i>',
    // },

    // add: {
    //   confirmCreate: true,
    // },
    add: false,
    edit: {
      editButtonContent: 'Edit',
      saveButtonContent: 'Update',
      cancelButtonContent: 'Cancel',
    },
    delete: {
      deleteButtonContent: 'Delete',
      confirmDelete: true,
    },
    columns: {

      id: {
        title: 'ID',
        type: 'number',
        filter: false,
      },
      CustomerName: {
        title: 'Name',
        type: 'string',
        filter: false,
      },
      NoItems: {
        title: 'No of Items',
        filter: false,
      },
      status: {
        title: 'Status',
        filter: false,
        editor: {
          type: 'list',
          config: {
            selectText: 'Select',
            list: [
              { value: 'Order Received', title: 'Order Received' },
              { value: 'Preparing', title: 'Preparing' },
              { value: 'Ready to serve', title: 'Ready to serve' },
            ],
          },
        },

      },
    },
  };
  source: LocalDataSource = new LocalDataSource();
  constructor() {
 
    let data = [
      {
        id: 1,
        CustomerName: "Sridharan",
        NoItems: "2",
        status: "Order Received",
        TotalAmount: 900,
        DeliveryAddress: "Box 564, Bangalore, India ",
        itemList: [{
          itemName: "item 1",
          price: 500,
        },
        {
          itemName: "item 2",
          price: 400,
        },]

      },
      {
        id: 2,
        CustomerName: "Saranya",
        NoItems: "3",
        DeliveryAddress: "Box 564, Bangalore, India ",
        status: "Preparing",
        TotalAmount: 1200,
        itemList: [{
          itemName: "item 1",
          price: 200,
        },
        {
          itemName: "item 2",
          price: 400,
        },
        {
          itemName: "item 2",
          price: 600,
        }]

      },
    ];
    this.source.load(data);
  }
  onCustom(event) {
    console.log(event);
    switch (event.action) {
      case 'ViewAction':
        this.openMyModalData(event.data);
        break;
    }
  }
  openMyModalData(formData: any) {
    this.modalDefault.show();
    console.log(formData)

    this.formData = formData;
    this.ItemList = formData.itemList

  }
  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
}
