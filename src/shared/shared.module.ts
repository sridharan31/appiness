import {NgModule, NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {ModalBasicComponent} from './modal-basic/modal-basic.component';




@NgModule({
  imports: [
    CommonModule,
    NgbModule,

  ],
  exports: [
    NgbModule,
    
    ModalBasicComponent,
    
  ],
  declarations: [

  
    ModalBasicComponent,
  
  ],
  providers: [

  ],
  schemas: [ NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA ],

})
export class SharedModule { }
